#ifndef STRUCTHEADER
#define STRUCTHEADER

typedef struct{
    char bezeichnung[20];
    double gehaltsfaktor; ///Damit bestimmte Jobs mehr bekommen
}jobStr;

typedef struct {
    char vorname[20];
    char nachname[20];
    unsigned int skills; ///Werte von 1-100
    unsigned int zufriedenheit; ///Werte von 1-100
    double stundenlohn;
    unsigned int alter;
    int arbeitet; ///falls er bei einer Filiale zugeteilt wurde 1; sonst 0
    jobStr job;
} mitarbeiter;

typedef struct {    char id[7];
    char name[20];
    double preis;    double verkaufsPreis;
    unsigned int anzahl;
    char einheit[10]; ///z.b. bei Wurst "10dag"
    unsigned int speziell; ///Wert zwischen 1 u. 5 wie speziell eine Ware ist (z.B. Wasser = 1; Pinkes Einhornwasser mit Zitronenaroma = 5)
    int kategorien[5];} waren;

typedef struct {
    char name[30];
    int einwohner;
    int mietpreis;
} city;
typedef struct{    int id;    char name[30];    char stadt[20];    int groesse;    waren **gelagerteWaren;    int anzahlGelagerteWaren;}lager_t;
typedef struct {
    char name[30];
    char stadt[20];
    int einw;
    int mietpreis;
    mitarbeiter *mitarbeiterInFiliale[100]; ///Pointer zu den Mitarbeitern die in der Filiale arbeiten
    int anzahlMitarbeiter;
    unsigned int ausbaustufe;
    waren handelswaren[200];    lager_t filialenLager;
} filialenStr;

typedef struct {
char *text;
int status;
} quest_t;typedef struct{    int id;    char name[64];    int position[5];    int katWarenAnz;    waren *wp[100]; ///warenpointerarray} kat_t;

#endif // STRUCTHEADER
