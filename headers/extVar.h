#include "structVar.h"

#define GUETER_ANZAHL 3
#define MAX_STRING_SIZE 30
#define ARRAY_SIZE 9
#define PRESSANYKEY printf("Beliebige Taste zum Fortsetzen druecken...");getch();
#define VORNAMEN 122
#define NACHNAMEN 1006
#define ERROR10 printf("\nError 10: Fehler beim Reservieren von Speicher. Programm wird nun beendet.\n");
#define ERROR22 printf("\nError 22: Fehler beim Auslesen einer Datei. Programm wird nun beendet.\n");
#define TOMANYLETTERS(max) printf("Es koennen maximal %d Zeichen eingegeben werden.",max);
#define INVALIDSIGN printf("Ungueltiges Zeichen.");

extern int tag,monat,jahr,ungBriefe; //"aktuelles Datum" aus "Datenbank" einlesen
extern waren *handelswaren;
extern int hwAnzahl;
extern waren *alleWaren;
extern int warenAnzahl;
extern kat_t *kategorien;
extern int katAnzahl;

extern int lagerAnzahl;

extern mitarbeiter *mitarbeiterArr;
extern mitarbeiter m[];
extern size_t mitarbeiterAnzahl;
extern jobStr beruf[];
extern int berufanzahl;

extern int gehalt; ///Die Variablen in dieser Zeile sind da, um mitarbeiterDetails einfacher verwenden zu k�nnen
extern double kapital;
extern double gewinntag;
extern char *vorname;
extern char *nachname;
extern char *firmenname;
extern double ustZahllast;

extern size_t filialenAnzahl;
extern filialenStr *filialenArr;
extern city staedte[];

extern char news[1000];

extern quest_t *quests;
extern int questi;
extern int questa[3];
