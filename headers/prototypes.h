#include "../headers/structVar.h"
#include "../headers/extVar.h"


int angestellte();
int angestellteScreen(int);
int einstellScreen(mitarbeiter m);
int mitarbeiterEinstellen();
int fehlertestmainMenu(char var);
int mainMenu(void);
int postmenu(int n);
int questinit();
int questmenu();
void questfree();
int saveGame();
int loadGame(char* name);
int createSave(char* savename, int savedata[], size_t values, char identifiers[][MAX_STRING_SIZE]);
int getSave(char* savename, int savedata[], size_t n, char identifiers[][MAX_STRING_SIZE]);
int countlines(FILE *fp);
int checkSave(FILE* savefile,size_t n,char identifiers[][MAX_STRING_SIZE]);
int getName();
int create(void);
void credits(void);
int neueFiliale(int nummer);
int filialeAnpassen(int nummer);
int mitarbeiterZuweisen(int nummer);
int filialen(void);
int gueterScreen(int *, char);
int gueter(int n);
int fehlertestmainGameMenu(char var);
char mainGameMenu(void);
void printHeader();
char* getNews(int type);
void simWarenverkauf();
int simNews(int type);
int parsestring(char* str);
int checkParseableString(char* str);
int countParseableElements(char* str);int generiereMitarbeiter(char *);int warenKategorienEinlesen(void);int warenliste();void freeGlobal(void);int pruefeEingabe(char,int,int);int lagerScreen(void);