#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "../headers/prototypes.h"

//Hallo Enk!

#define Z putchar(178);
#define B putchar(' ');
#define E putchar('\n');


int mainMenu(void) {
    char eingabe;

    Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z E
    Z Z Z Z Z B B B Z B Z B Z B Z B B B Z Z Z Z Z E
    Z Z Z Z Z B Z Z Z B Z B Z B Z B Z Z Z Z Z Z Z E
    Z Z Z Z Z B Z Z Z B Z B Z B Z B B B Z Z Z Z Z E
    Z Z Z Z Z B Z Z Z B Z B Z B Z Z Z B Z Z Z Z Z E
    Z Z Z Z Z B B B Z B B B B B Z B B B Z Z Z Z Z E
    Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z Z E
    printf("C-Wirtschaftssimulation\n\n");

#ifndef DEBUG
    printf("\n>n... Neues Spiel\n>l... Spiel laden\n>c... Credits\n>b... Beenden\n\n>");
#endif // DEBUG
#ifdef DEBUG
    printf("\n>n... Neues Spiel\n>l... Spiel laden\n>d... Spiel mit Debug laden\n>c... Credits\n>b... Beenden\n\n>");
#endif // DEBUG

    do {
        eingabe = (char)getch();
    } while(!fehlertestmainMenu(eingabe));

    return eingabe;
}

int fehlertestmainMenu(char var) {

#ifndef DEBUG
    if(var == 'n' || var == 'b' || var == 'l' || var == 'c')
#endif // DEBUG
#ifdef DEBUG
        if(var == 'n' || var == 'b' || var == 'l' || var == 'c' || var == 'd')
#endif // DEBUG
            return 1;

    return 0;
}
