#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "../headers/extVar.h"
#include "../headers/prototypes.h"

char mainGameMenu(void) {
    char eingabe;


    system("cls");

    printHeader();
    printf(">p... Posteingang (%d)\n>f... Filialen\n>g... Gueter\n>a... Angestellte\n>l... Lager\n>q... Quests\n\n>w... Weiter\n>s... Spiel speichern\n>b... beenden\n\n>",ungBriefe);

    do {
        fflush(stdin);
        eingabe = (char)getch();
    } while(!fehlertestmainGameMenu(eingabe));
    return eingabe;
}

int fehlertestmainGameMenu(char var) {

    if(var == 'a' || var == 'p' || var == 'w' || var == 'b' || var == 'g' || var == 's' || var == 'q' || var == 'f' || var == 'l') {
        return 1;
    }
    return 0;
}
