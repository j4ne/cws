#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../headers/extVar.h"
#include "../headers/prototypes.h"


char names[ARRAY_SIZE][MAX_STRING_SIZE];

int saveGame()
{

    int i=0;




    char* name = calloc(sizeof(char),strlen(firmenname)+4);
    strcpy(name,"save/");
    strncat(name,firmenname,strlen(firmenname)-5);
    strcat(name,".sav");

    int data[ARRAY_SIZE];
    data[i] = tag;
    strcpy(names[i++],"Tag");
    data[i] = monat;
    strcpy(names[i++], "Monat");
    data[i] = jahr;
    strcpy(names[i++], "Jahr");
    data[i] = kapital;
    strcpy(names[i++], "Kapital");
    data[i] = ungBriefe;
    strcpy(names[i++], "Ungelesene_Briefe");
    data[i] = mitarbeiterAnzahl;
    strcpy(names[i++], "Mitarbeiter");

    for(;i<GUETER_ANZAHL;i++)
    {

        data[i+6]=handelswaren[i].anzahl;
        strcpy(names[i+6],handelswaren[i].name);

    }

    return createSave(name,data,sizeof(data)/sizeof(data[0]),names);

}

int createSave(char* savename, int savedata[], size_t values, char identifiers[][MAX_STRING_SIZE])
{

    FILE *savefile = fopen(savename,"w");  //File �ffnen
    int i;

    for(i=0; i<values; i++)
        fprintf(savefile,"%s: %d\n",identifiers[i],savedata[i]);

    fclose(savefile);
    return 0;

}

int loadGame(char* name)
{

    int data[ARRAY_SIZE],i=0;
    char* nameShort = calloc(sizeof(char),strlen(name)-5);
    strcpy(nameShort,"save/");
    strncat(nameShort,name,strlen(firmenname)-5);
    strcat(nameShort,".sav");

    if(getSave(nameShort,data,ARRAY_SIZE,names))
        return 1;

    tag=data[i++];
    monat=data[i++];
    jahr=data[i++];
    kapital=data[i++];
    ungBriefe=data[i++];
    mitarbeiterAnzahl=data[i++];

    for(;i<ARRAY_SIZE;i++)
        handelswaren[i-6].anzahl = data[i];

    free(nameShort);
    return 0;
}

int getSave(char* savename, int savedata[], size_t n, char identifiers[][MAX_STRING_SIZE])
{

    FILE *savefile = fopen(savename,"r");
    fpos_t pos;
    fgetpos(savefile,&pos);

    if(savefile==NULL)
        return 1;

    int i;
    char trash[MAX_STRING_SIZE];

//    if(lines != n+1 || checkSave(savefile,n,identifiers)){
//        printf("Fehler! Erwartete Zeilen:%d Zeilen im File: %d\n",n,lines);
//        fflush(stdin);
//        getchar();
//        return -1;}

    fsetpos(savefile, &pos);

    for(i=0; i<n; i++)
    {
        fscanf(savefile,"%s%d\n",trash,&(savedata[i]));
        printf("Eingelesener Wert: %s%d\n",trash,savedata[i]);
    }

    fclose(savefile);
    return 0;

}

int countlines(FILE *fp)
{
    fseek(fp,0,0);
    int lines=0;
    while(!feof(fp)){
        if(fgetc(fp)=='\n')
            lines++;
    }
    fseek(fp,0,0);
    return lines;
}

int checkSave(FILE* savefile,size_t n,char identifiers[][MAX_STRING_SIZE])
{

    int i,trash;
    char** tmp = calloc(sizeof(char),MAX_STRING_SIZE*n);

    for(i=0; i<n; i++)
    {

        if(fscanf(savefile,"%s: %d\n",tmp[i],&trash)!=2)
            return -1;

        if(tmp[i]!=identifiers[i])
            return -1;


    }
    return 0;
}
