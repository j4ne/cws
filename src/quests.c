#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <string.h>
#include "../headers/structVar.h"
#include "../headers/prototypes.h"

#define ANZQ 3

int questmenu(){
int i, f=0;
char z;
if(!questi){
        puts("test2");
    if(!questinit()){
        printf("ein Fehler ist aufgetreten\n");                     //evtl. Peppis Fehlercodes
        return 0;
    }
}




system("cls");
printHeader();
printf("Die Quests sollen den Einstieg ins Spiel erleichtern.\n\n");

for(i=0;i<ANZQ;i++){

    printf("%d. Quest: %-30s           ", i+1, quests[i].text);

     if(questa[i]&&!quests[i].status){
        printf("abgeschlossen / Belohnung abholen\n");
        quests[i].status=1;
        f++;
     }else if(quests[i].status){
        printf("abgeschlossen\n");
     }else{
        printf("ausstehend\n");
     }
}
if(f){
    printf("\n>z... zurueck und Belohnung abholen\n");
}else{
    printf("\n>z... zurueck\n");
}

do{
    printf("\n>");
    z = getch();
}while(z!='z');
if(f){
   kapital+=f*100000; //Belohnung
}
return 1;
}

void questfree(){
int i;
if(questi){
    for(i=0;i<ANZQ;i++){
        free(quests[i].text);
    }
    free(quests);
}
}

int questinit(){
int i;
char str[128]="";
FILE *fp=NULL;
fp=fopen("data/quests.dat","r");
if(!fp){
    return -1;
}
   quests = calloc(sizeof(quest_t),ANZQ);

for(i=0;i<ANZQ;i++){

    fgets(str,128,fp);
    quests[i].text = calloc(sizeof(char),strlen(str)+1);
    strcpy(quests[i].text,str);
    quests[i].text[strlen(str)-1] = '\0';
    quests[i].status = 0;

}
rewind(fp);
fclose(fp);
questi = 1;
return 1;
}
