#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include "../headers/extVar.h"
#include "../headers/prototypes.h"

///Grundidee:
///ansicht aller filialen
///mit z kommt man zur�ck
///
///(josef; 29.09.)

int filialen(void) {
    int i;
    char eingabe;

    do {
        system("cls");
        printHeader();

        printf("      %-30s %-20s Einwohner\n","Name der Filiale","Stadt");
        for(i=0; i<filialenAnzahl; i++)
            printf(">%d... %-30s %-20s %d\n",i+1,filialenArr[i].name,filialenArr[i].stadt,filialenArr[i].einw);

        printf("\n\n>n... neue Filiale\n>z... zurueck\n\n>");

        eingabe = getch();
        if(eingabe == 'n'){
            if(neueFiliale(i-1)==-1)                return -1;        }
        else if(eingabe >= '1' && eingabe-'0' <= filialenAnzahl)
            filialeAnpassen(eingabe-'0'-1);


    } while(eingabe!='z');
    return 0;
}

int neueFiliale(int nummer) {
    int i,fehler;
    char filName[20];
    char eingabe, eingabe1;
    system("cls");
    printHeader();

    for(i=0; i<3; i++)
        printf(">%d... %-10s %-d (%d Euro/Monat)\n",i+1,staedte[i].name,staedte[i].einwohner,staedte[i].mietpreis);
    printf(">z... zurueck\n\n>");

    do {
        do {
            fehler = 0;
            eingabe = getch();
            if(eingabe<'1' || eingabe>'3')
                fehler = 1;
        } while(fehler);
        fehler = 0;
        do {
            fehler = 0;
            printf("Neuer Name: ");
            gets(filName);
            ///�berpr�fung der Eingabe
            for(i=0; i<strlen(filName); i++) {
                if((filName[i]<'A' || filName[i]>'z') && filName[i] != 39 && filName[i] != ' '  && (filName[i]<'0' || filName[i]>'9')) {
                    fehler = 1;
                    printf("Ungueltige Eingabe.\n");
                }
            }
        } while(fehler);
        if(eingabe>='1' && eingabe<='3') {
            fflush(stdin);
            printf("Wollen Sie die Filiale %s in %s errichten? (j/n)\n>",filName,staedte[eingabe-'1'].name);
            eingabe1 = getch();
            if(eingabe1=='j') { ///Die eingegebenen Daten werden in der Fillialen Array gespeichert                filialenArr = realloc(filialenArr,filialenAnzahl+1);                if(filialenArr == NULL){                    ERROR10                    PRESSANYKEY                    return -1;                }
                strcpy(filialenArr[filialenAnzahl].stadt,staedte[eingabe-'1'].name);
                strcpy(filialenArr[filialenAnzahl].name,filName);
                filialenArr[filialenAnzahl].einw = staedte[eingabe-'1'].einwohner;
                filialenArr[filialenAnzahl].mietpreis = staedte[eingabe-'1'].mietpreis;
                filialenArr[filialenAnzahl].anzahlMitarbeiter = 0;
                filialenArr[filialenAnzahl].ausbaustufe = 1;
                strcpy(filialenArr[filialenAnzahl].filialenLager.name, filName);                strcpy(filialenArr[filialenAnzahl].filialenLager.stadt, staedte[eingabe-'1'].name);                filialenArr[filialenAnzahl].filialenLager.id = lagerAnzahl++;                filialenAnzahl++;
                questa[0]=1; //erste Quest

                return 1;
            } else if(eingabe1=='n')
                return 0;
        }
    } while(eingabe!='z');
    return 0;
}

int filialeAnpassen(int nummer) { ///Um den Namen der Filiale �ndern zu k�nnen und bestimmte Mitarbeiter zuweisen zu k�nnen
    int fehler = 0,i;
    char teststring[64] = "";
    char eingabe,eingabe2;


    do {
        system("cls");
        printHeader();

        printf("Name: %s\nStadt: %s\nMiete/Monat: %d\nAusbaustufe: %d\n\n>m... Mitarbeiter zuweisen\n>n... Name aendern\n>a... Filiale ausbauen\n>l... Lager\n\n>z... zurueck\n\n>",filialenArr[nummer].name,filialenArr[nummer].stadt,filialenArr[nummer].mietpreis,filialenArr[nummer].ausbaustufe);
        eingabe = getch();
        if(eingabe=='n') {
            fehler = 0;
            do {
                fehler = 0;
                printf("Neuer Name: ");
                gets(teststring);
                for(i=0; i<strlen(teststring); i++) {
                    if((teststring[i]<'A' || teststring[i]>'z') && teststring[i] != 39 && teststring[i] != ' '  && (teststring[i]<'0' || teststring[i]>'9')) {
                        fehler = 1;
                        printf("Ungueltige Eingabe.\n");
                    }
                }
            } while(fehler);
            strcpy(filialenArr[nummer].name,teststring);
            return 1;
        } else if(eingabe=='m') {
            mitarbeiterZuweisen(nummer);
        } else if(eingabe=='a') {
            printf("Wollen Sie die Filiale %s in %s wirklich um %d Euro ausbauen? (j/n)\n>",filialenArr[nummer].name,filialenArr[nummer].stadt,filialenArr[nummer].mietpreis*10);
            do {
                eingabe = getch();
                if(eingabe=='j') {
                    filialenArr[nummer].ausbaustufe++;
                    kapital -= filialenArr[nummer].mietpreis*10;
                    return 1;
                }
            } while(eingabe != 'n');
            return 0;
        } else if(eingabe=='l') {
            do{
            system("cls");
            printHeader();
                for(i=0; i<GUETER_ANZAHL; i++){
                    if(filialenArr[nummer].handelswaren[i].anzahl) ///Alle Waren, die man besitzt, werden ausgegeben.
                        printf("%s %lf %d\n",filialenArr[nummer].handelswaren[i].name,filialenArr[nummer].handelswaren[i].preis,filialenArr[nummer].handelswaren[i].anzahl);
                }
                printf("\n>z... zurueck\n\n>");
                eingabe2 = getch();
            }while(eingabe2!='z');
        }
    } while(eingabe!='z');

    return 0;
}

int mitarbeiterZuweisen(int nummer) {
    int i,fehler,check,eingabe;    char eingabeString[2];
    char ch;

    system("cls");
    printHeader();
    printf("Freie Mitarbeiter:\n");
    for(i=0; i<mitarbeiterAnzahl; i++) {
        if(!mitarbeiterArr[nummer].arbeitet)
            printf(">%d... %s %s\n",i+1,mitarbeiterArr[i].vorname,mitarbeiterArr[i].nachname);
    }
    printf("\n>z... zurueck\n\n>");
    do {
        fehler = 0;
        ch = ' ';
        check = scanf("%2s%c",eingabeString,&ch);
        if(ch!='\n' || check != 2)
            fehler = 1;        eingabe = atoi(eingabeString);
        if(eingabe>=1 && eingabe<=mitarbeiterAnzahl)
            filialenArr->mitarbeiterInFiliale[(filialenArr[nummer].anzahlMitarbeiter)++] = &mitarbeiterArr[eingabe-1];        else if(eingabeString[0]=='z')            return 1;

    } while(fehler);

    return 0;
}
