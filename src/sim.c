#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "../headers/extVar.h"
#include "../headers/prototypes.h"
#define STARTTOKEN '{'
#define ENDTOKEN '}'
#define SEPERATOR ';'
#define FILESTARTTOKEN '['
#define FILEENDTOKEN ']'
#define NUMBERSTARTTOKEN '<'
#define NUMBERENDTOKEN '>'

#define NEWSZEILE 35 //Falls man nur eine Bestimmte Zeile Ausgeben will

///Hier stehen alle Funktionen, die etwas mit der Simulation zu tun haben. :P
///Josef, 9.10.2018

void simWarenverkauf() {
//    int i,j,k;
//    double zahl;
//
//    srand((unsigned)time(NULL));





}
int simNews(int type) {
    int i,linenum,n=0;
    char alleNews[1000000] = "";
    char *newsbuf = calloc(1000,sizeof(char));
    FILE *fp;
    char name[20];

    //srand((unsigned)time(NULL));    //srand ist schon im Main einmal aufgerufen worden

    switch(type) {

    case 0:
        strcpy(name,"data/randnews.dat");
        break;
    case 1:
        strcpy(name,"data/neutnews.dat");
        break;
    case 2:
        strcpy(name,"data/posnews.dat");
        break;
    case 3:
        strcpy(name,"data/negnews.dat");
        break;
    default:
        strcpy(news,"Fehler! Falscher Nachrichtentyp");
        return 1;
    }

    fp = fopen(name,"r");
    if(fp==NULL)
        return 0;

    while(!feof(fp)) {
        fgets(newsbuf,1000,fp);
        strcat(alleNews,newsbuf);
        n++;
    }

    fclose(fp);
    free(newsbuf);
    #ifdef NEWSZEILE
    linenum=NEWSZEILE;
    #endif // NEWSZEILE
    #ifndef NEWSZEILE
    linenum=rand()%n+1;
    #endif


    newsbuf = strtok(alleNews,"\n");

    for(i=1; i<linenum; i++)
        newsbuf = strtok(NULL,"\n");


    strcpy(news,newsbuf);

    switch(parsestring(news)) {
    //Fehlerbehandlung in funktion
    default:
        break;
    }
    return 1;
}

int parsestring(char* str) {

    size_t n = strlen(str),elements;
    char tmpstr[500]="";
    char tmpstr2[500]="";
    char* element;
    char extfile[100]="";
    int i,j,k,start=-1,stop=-1,maxrz,minrz;
    FILE *fp = NULL;

//    if(!checkParseableString(str))
//        return 1;

    //Im moment wird nur auf eine Expression geprüft (jetzt nicht mehr)
    for(i=0; i<n; i++) {
        if(str[i]==STARTTOKEN) {
            start=i+1;
            for(j=i; j<n; j++) {
                if(str[j]==ENDTOKEN) {
                    stop=j+1;
                    strncpy(tmpstr,str+start, stop-start);

                    elements=countParseableElements(tmpstr);
                    int elementnr=rand()%elements+1;
                    element=strtok(tmpstr, ";");
                    for(k=1; k<elementnr; k++)
                        element=strtok(NULL, ";");

                    strncpy(tmpstr2,str+stop,strlen(str)-(stop-start));
                    strcpy(str+start-1,element);
                    strcat(str,tmpstr2);

                    break;
                }
            }
            //break;
        }
    }

    //Auf externen Fileverweis pruefen
    n = strlen(str);
    for(i=0; n>i; i++) {
        if(str[i]==FILESTARTTOKEN) {
            start=i+1;
            for(; n>i; i++) {
                if(str[i]==FILEENDTOKEN) {
                    stop=i;
                    strncpy(extfile,str+start,stop-start);

                    fp = fopen(extfile,"r");
                    if(!fp) {
                        char *tmpnews = calloc(sizeof(char),strlen(str)+1);
                        strcpy(tmpnews,str);
                        sprintf(news,"Fehler! Externe Datei %s konnte nicht geoeffnet werden\n%s",extfile,tmpnews);
                        free(tmpnews);
                        return 2;
                    }
                    n=countlines(fp);
                    for(i=rand()%n; i>0;) {
                        if(fgetc(fp)=='\n')
                            i--;
                    }
                    fgets(tmpstr,100,fp);

                    if(tmpstr[strlen(tmpstr)-1]=='\n')
                        tmpstr[strlen(tmpstr)-1]='\0';

                    strncpy(tmpstr2,str+stop+1,strlen(str)-(stop-start));
                    strcpy(str+start-1,tmpstr);
                    strcat(str,tmpstr2);
                    fclose(fp);
                    break;
                }
            }
            //break;
        }
    }

    //Randomzahlverweis
    n = strlen(str);
    for(i=0; i<n; i++) {
        //printf("%c = %c --> %d\n",str[i],NUMBERSTARTTOKEN,str[i]==NUMBERSTARTTOKEN);
        if(str[i]==NUMBERSTARTTOKEN) {
            start=i+1;
            for(j=i; n>j; j++) {
                if(str[j]==NUMBERENDTOKEN) {
                    stop=j;
                    strncpy(tmpstr,str+start,stop-start);
                    tmpstr[stop-start]='\0';
                    if(sscanf(tmpstr,"%d;%d",&minrz,&maxrz)!=2) {
                        char *tmpnews = calloc(sizeof(char),strlen(str)+1);
                        strcpy(tmpnews,str);
                        sprintf(news,"Fehler beim auswerten des Ausdrucks %s\n%s",tmpstr,tmpnews);
                        free(tmpnews);
                        return 3;
                    }

                    k=rand()%(maxrz-minrz)+minrz;

                    strncpy(tmpstr2,str+stop+1,strlen(str)-(stop-start));
                    sprintf(str+start-1,"%d",k);
                    strcat(str,tmpstr2);
                    break;

                }

            }

        }

    }

    return 0;
}
int checkParseableString(char* str) {

    int fileopen=0,open=0,i;
    for(i=0; i<strlen(str); i++) {

        if(str[i]==STARTTOKEN&&!open)
            open=1;
        else if(str[i]==STARTTOKEN&&open)
            return 0;
        else if(str[i]==ENDTOKEN&&open)
            open=0;
        else if(str[i]==ENDTOKEN&&!open)
            return 0;

        if(str[i]==FILESTARTTOKEN&&!fileopen)
            fileopen=1;
        else if(str[i]==FILESTARTTOKEN&&fileopen)
            return 0;
        else if(str[i]==FILEENDTOKEN&&fileopen)
            fileopen=0;
        else if(str[i]==ENDTOKEN&&!fileopen)
            return 0;

    }
    //Mehr Checks hier einfügen
    //Test

    return 1;
}

int countParseableElements(char* str) {

    int sum = 0,i;

    for(i=0; strlen(str)>i; i++) {
        if(str[i]==SEPERATOR)
            sum++;
    }
    return sum;
}



int generiereMitarbeiter(char *berufFoo) {
    int i,j=0,n1;
    FILE *fp1, *fp2;
    n1 = rand()%VORNAMEN+1;
    fp1 = fopen("data/vornamen.dat","r");
    fp2 = fopen("data/nachnamen.dat","r");
    if(!fp1 || !fp2) {
        printf("Fehler beim �ffnen einer Datei. Programm wird nun beendet.");
        return 1;
    }

    for(i=0; i<n1; i++)
        fscanf(fp1,"%s\n",m[0].vorname);
    n1 = rand()%VORNAMEN+1;
    rewind(fp1);

    for(i=0; i<n1; i++)
        fscanf(fp1,"%s\n",m[1].vorname);
    n1 = rand()%VORNAMEN+1;
    rewind(fp1);

    for(i=0; i<n1; i++)
        fscanf(fp1,"%s\n",m[2].vorname);
    n1 = rand()%NACHNAMEN+1;

    for(i=0; i<n1; i++)
        fscanf(fp2,"%s\n",m[0].nachname);
    n1 = rand()%NACHNAMEN+1;
    rewind(fp2);

    for(i=0; i<n1; i++)
        fscanf(fp2,"%s\n",m[1].nachname);
    n1 = rand()%NACHNAMEN+1;
    rewind(fp2);
    for(i=0; i<n1; i++)
        fscanf(fp2,"%s\n",m[2].nachname);

    ///Berechnung von Geh�ltern, Skills,...
    for(i=0; i<3; i++) {
        m[i].alter = rand()%30+20; ///Der Mitarbeiter soll zwischen 20 u. 50 Jahren alt sein
        m[i].zufriedenheit = 50;
        m[i].skills = m[i].alter/2*(rand()%4+1); ///Die Skills h�ngen vom Alter ab und liegen zwischen 10 und 100
        while(1) {
            if(!strcmp(berufFoo,beruf[j].bezeichnung)) {
                m[i].stundenlohn = m[i].skills*((double)(rand()%100+50)/100)*beruf[j].gehaltsfaktor; ///Das Gehalt h�ngt von den Skills ab (min: 0.5*skills; max: 1.5*skills), sp�ter kommt dann noch ein Jobabh�nginger Multiplikator dazu
                break;
            }
            j++;
        }
        strcpy(m[i].job.bezeichnung, berufFoo);
    }

    fclose(fp1);
    fclose(fp2);

    return 0;
}
