#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <time.h>
#include <string.h>
#include "../headers/extVar.h"
#include "../headers/prototypes.h"

///Grundidee:
///ansicht des personals
///mit z kommt man zur�ck
///
///(josef; 22.09.)


int angestellte() {
    int i;
    char eingabe = '\0';

    do {
        system("cls");
        printHeader();

        for(i=0; i<mitarbeiterAnzahl; i++) { ///n ist die Anzahl der verschiedenen G�ter
            printf(">%d - %s %s\n",i+1,mitarbeiterArr[i].vorname,mitarbeiterArr[i].nachname);
        }
        printf("\n>n... neuen Mitarbeiter einstellen\n\n>z... zurueck\n\n");
        eingabe = getch();
        if(eingabe=='z')
            return 0;
        else if(eingabe-48 <= mitarbeiterAnzahl && eingabe-48 > 0)
            angestellteScreen(eingabe-48);
        else if(eingabe=='n') {
            mitarbeiterEinstellen();
        }
    } while(1);

    return 0;
}

int angestellteScreen(int nummer) { ///Hier wird f�r jedes Gut (EZ von G�ter :p) ein spezieller Screen angezeigt, falls dieses ausgew�hlt wurde
    int check;
    char eingabe,ch;
    double gehaltNEW;

    system("cls");
    printHeader();

    do {
        system("cls");
        printHeader();
        printf("--%s %s--\nGehalt: %.2lf Euro/Stunde\nZufriedenheit: %d\nSkills: %d\n",mitarbeiterArr[nummer-1].vorname,mitarbeiterArr[nummer-1].nachname,mitarbeiterArr[nummer-1].stundenlohn,mitarbeiterArr[nummer-1].zufriedenheit,mitarbeiterArr[nummer-1].skills);


        printf("\n>g... Gehalt anpassen\n\n>z... zurueck\n\n>");
        eingabe = getch();
        if(eingabe=='z')
            return 0;
        else if(eingabe=='g') {
            ///Hier werden die Anzahl der nachzubestellenden Gueter eingegeben
            do {
                fflush(stdin);
                ch = ' ';
                printf("Neuen Stundenlohn (in Euro) eingeben: ");
                check = scanf("%lf%c",&gehaltNEW,&ch);
                if(gehaltNEW > 0 && check == 2 && ch == '\n') {
                    printf("Soll das Gehalt von %s %s von %.2lf auf %.2lf Euro/Stunde erhoeht werden? (j/n)\n>",mitarbeiterArr[nummer-1].vorname,mitarbeiterArr[nummer-1].nachname,mitarbeiterArr[nummer-1].stundenlohn,gehaltNEW);
                    ///�berpr�fung des Eingegebenen
                    do {
                        eingabe = getch();
                        if(eingabe == 'j') {
                            mitarbeiterArr[nummer-1].zufriedenheit *= (double)(gehaltNEW/mitarbeiterArr[nummer-1].stundenlohn);
                            if(mitarbeiterArr[nummer-1].zufriedenheit>100)
                                mitarbeiterArr[nummer-1].zufriedenheit=100;
                            mitarbeiterArr[nummer-1].stundenlohn = gehaltNEW;
                            return 0;
                        } else if(eingabe == 'n')
                            return 1;
                    } while(1);
                }
            } while(1);
        }
    } while(1);
}

int mitarbeiterEinstellen() {
    int i;
    char eingabe;
    char *berufFoo;

    system("cls");
    printHeader();

    for(i=0; i<berufanzahl; i++)
        printf("%d - %s\n",i+1,beruf[i].bezeichnung);
    printf("\n>z... zurueck\n\n>");
    do{
        eingabe = getch();
        if(eingabe>='1' && eingabe<='0'+berufanzahl){
            berufFoo = calloc(strlen(beruf[eingabe-'1'].bezeichnung),sizeof(char));
            if(berufFoo == NULL){
                printf("FEHLER!");
                return 1;
            }
            strcpy(berufFoo,beruf[eingabe-'1'].bezeichnung);
            break;
        }
        else if(eingabe=='z')
            return 1;


    }while(1);

    do {
        system("cls");
        printHeader();
        puts(berufFoo);
        for(i=0; i<3; i++)
            printf(">%d: %s %s (%d)\n",i+1,m[i].vorname,m[i].nachname,m[i].alter);
        printf("\n>z... zurueck\n\n>");

        fflush(stdin);
        eingabe = getch();
        if(eingabe>='1' && eingabe<='3') {
            einstellScreen(m[eingabe-'0'-1]);
            return 1;
        }


    } while(eingabe!='z');
    return 1;
}

int einstellScreen(mitarbeiter m) {
    char eingabe;
    system("cls");
    printHeader();
    printf("%s\n%s\n\nAlter: %d\nStundenlohn: %.2lf\nSkills: %d\n\n\n>e... einstellen\n>z... zurueck\n\n>",m.vorname,m.nachname,m.alter,m.stundenlohn,m.skills);
    do {
        eingabe = getch();
        if(eingabe=='e') {
            do {
                printf("Wollen Sie wirklich %s %s einstellen? (j/n)\n>",m.vorname,m.nachname);
                eingabe = getch();
                if(eingabe=='j') {
                    if(!mitarbeiterAnzahl) {
                        mitarbeiterArr=malloc(sizeof(mitarbeiter));
                        if(mitarbeiterArr==NULL) {
                            printf("Fehler! Kein Speicher verf�gbar!");
                            return -1;
                        }

                    }

                    else if(realloc(mitarbeiterArr,mitarbeiterAnzahl*sizeof(mitarbeiter))==NULL) {
                        printf("Fehler! Kein Speicher verf�gbar!");
                        return -1;
                    }

                    mitarbeiterAnzahl++;
                    strcpy(mitarbeiterArr[mitarbeiterAnzahl-1].vorname,m.vorname);
                    strcpy(mitarbeiterArr[mitarbeiterAnzahl-1].nachname,m.nachname);
                    mitarbeiterArr[mitarbeiterAnzahl-1].alter = m.alter;
                    mitarbeiterArr[mitarbeiterAnzahl-1].skills = m.skills;
                    mitarbeiterArr[mitarbeiterAnzahl-1].zufriedenheit = m.zufriedenheit;
                    mitarbeiterArr[mitarbeiterAnzahl-1].stundenlohn = m.stundenlohn;
                    mitarbeiterArr[mitarbeiterAnzahl-1].arbeitet = 0;
                    strcpy(mitarbeiterArr[mitarbeiterAnzahl-1].job.bezeichnung,m.job.bezeichnung);
                    return 1;
                }
            } while(eingabe!='n');
            return 0;
        }
    } while(eingabe!='z');
    return 0;
}
